﻿using System.Threading.Tasks;

namespace Cleverbit.Task.Utilities
{
    public interface IHashService
    {
        Task<string> HashText(string plainText);
    }
}
