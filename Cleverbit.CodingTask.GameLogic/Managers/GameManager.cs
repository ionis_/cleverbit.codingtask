﻿using Cleverbit.CodingTask.GameLogic.Helpers;
using Cleverbit.Task.Data.Models;
using Cleverbit.Task.GameLogic.Accessors;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cleverbit.Task.GameLogic.Managers
{
    public class GameManager
    {
        private readonly GameAccessor _gameAccessor;
        private static readonly Random _rnd = new Random(Guid.NewGuid().GetHashCode());

        public GameManager(GameAccessor gameAccessor)
        {
            _gameAccessor = gameAccessor;
        }

        public async Task<int> Play(int userId)
        {
            GameMatch match = _gameAccessor.GetCurrentMatch();

            if (match == null || DateTime.Now > match.ExpiringAt)
            {
                match = await _gameAccessor.StartNewMatch();
            }

            var number = _rnd.Next(1, 100);

            _gameAccessor.UpdateMatch(match.Id, new PlayedNumber
            {
                UserId = userId,
                Number = number
            });

            return number;
        }

        public List<GameMatch> ShowGames()
        {
            return _gameAccessor.ShowGames();
        }
    }
}
