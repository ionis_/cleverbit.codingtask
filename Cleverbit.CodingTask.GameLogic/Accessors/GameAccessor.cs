﻿using Cleverbit.CodingTask.GameLogic.Helpers;
using Cleverbit.Task.Data;
using Cleverbit.Task.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cleverbit.Task.GameLogic.Accessors
{
    public class GameAccessor
    {
        private readonly CodingTaskContext _dbContext;

        public GameAccessor(CodingTaskContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<GameMatch> StartNewMatch()
        {
            GameMatch match = GameFactory.Create();
            _dbContext.Matches.Add(match);
            await _dbContext.SaveChangesAsync();
            return match;
        }

        public async void UpdateMatch(int matchId, PlayedNumber number)
        {
            GameMatch match = await _dbContext.Matches.FindAsync(matchId);

            if (match == null)
            {
                throw new KeyNotFoundException($"match {matchId} not found");
            }

            match.Numbers.Add(number);
            await _dbContext.SaveChangesAsync();
        }

        public GameMatch GetCurrentMatch()
        {
            return _dbContext.Matches.OrderByDescending(x => x.ExpiringAt).FirstOrDefault();
        }

        public List<GameMatch> ShowGames()
        {
            return _dbContext.Matches
                .Include(x=> x.Numbers)

                .Where(x => x.ExpiringAt < DateTime.Now)
                .OrderByDescending(x => x.ExpiringAt)
                .ToList();
        }
    }
}
