﻿using Cleverbit.Task.GameLogic.Accessors;
using Cleverbit.Task.GameLogic.Managers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cleverbit.CodingTask.GameLogic.Helpers
{
    public static class GameConfigServiceCollectionExtensions
    {
        public static IServiceCollection AddGameDependencies(this IServiceCollection services)
        {
            services.AddScoped<GameManager, GameManager>();
            services.AddScoped<GameAccessor, GameAccessor>();

            return services;
        }
    }
}
