﻿using Cleverbit.Task.Data.Models;
using System;
using System.Collections.Generic;

namespace Cleverbit.CodingTask.GameLogic.Helpers
{
    public static class GameFactory
    {
        public static GameMatch Create()
        {
            return new GameMatch()
            {
                ExpiringAt = DateTime.Now.AddMinutes(1),
                Numbers = new List<PlayedNumber>()
            };
        }
    }
}
