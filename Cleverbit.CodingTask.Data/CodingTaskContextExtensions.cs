﻿using Cleverbit.Task.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cleverbit.Task.Data
{
    public static class CodingTaskContextExtensions
    {
        private static readonly Random _rnd = new Random(Guid.NewGuid().GetHashCode());
        public static async System.Threading.Tasks.Task Initialize(this CodingTaskContext context, IHashService hashService)
        {
            await context.Database.EnsureCreatedAsync();

            var currentUsers = await context.Users.ToListAsync();

            bool anyNewUser = false;

            if (!currentUsers.Any(u => u.UserName == "User1"))
            {
                context.Users.Add(new Models.User
                {
                    UserName = "User1",
                    Password = await hashService.HashText("Password1")
                });

                anyNewUser = true;
            }

            if (!currentUsers.Any(u => u.UserName == "User2"))
            {
                context.Users.Add(new Models.User
                {
                    UserName = "User2",
                    Password = await hashService.HashText("Password2")
                });

                anyNewUser = true;
            }

            if (!currentUsers.Any(u => u.UserName == "User3"))
            {
                context.Users.Add(new Models.User
                {
                    UserName = "User3",
                    Password = await hashService.HashText("Password3")
                });

                anyNewUser = true;
            }

            if (!currentUsers.Any(u => u.UserName == "User4"))
            {
                context.Users.Add(new Models.User
                {
                    UserName = "User4",
                    Password = await hashService.HashText("Password4")
                });

                anyNewUser = true;
            }

            if (anyNewUser)
            {
                await context.SaveChangesAsync();
            }

            var users = context.Users.ToList();

            context.Matches.Add(new Models.GameMatch
            {
                ExpiringAt = DateTime.Now.AddDays(-1),
                Numbers = new List<Models.PlayedNumber>
                {
                    new Models.PlayedNumber
                    {
                        Number= _rnd.Next(1, 100),
                        User = users[0],
                        UserId = users[0].Id
                    },
                    new Models.PlayedNumber
                    {
                        Number= _rnd.Next(1, 100),
                        User = users[1],
                        UserId = users[1].Id
                    },
                    new Models.PlayedNumber
                    {
                        Number= _rnd.Next(1, 100),
                        User = users[2],
                        UserId = users[2].Id
                    },
                    new Models.PlayedNumber
                    {
                        Number= _rnd.Next(1, 100),
                        User = users[3],
                        UserId = users[3].Id
                    }
                }
            });

            await context.SaveChangesAsync();
        }
    }
}
