﻿using Cleverbit.Task.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Cleverbit.Task.Data
{
    public class CodingTaskContext : DbContext
    {
        public CodingTaskContext(DbContextOptions<CodingTaskContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<GameMatch> Matches { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable(nameof(User));
            modelBuilder.Entity<GameMatch>().ToTable(nameof(GameMatch));
            modelBuilder.Entity<PlayedNumber>().ToTable(nameof(PlayedNumber));
        }
    }
}
