﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cleverbit.Task.Data.Models
{
    public class GameMatch
    {
        public int Id { get; set; }
        public DateTime ExpiringAt { get; set; }
        public virtual List<PlayedNumber> Numbers { get; set; }

        public PlayedNumber Winner
        {
            get
            {
                return Numbers.OrderByDescending(x => x.Number).FirstOrDefault();
            }
        }
    }
}
