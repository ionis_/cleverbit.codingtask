﻿namespace Cleverbit.Task.Data.Models
{
    public class PlayedNumber
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public int MatchId { get; set; }
        public virtual GameMatch Match { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
    }
}
