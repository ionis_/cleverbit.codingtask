﻿using Cleverbit.Task.Data.Models;
using Cleverbit.Task.GameLogic.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cleverbit.CodingTask.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameController : ControllerBase
    {
        private readonly GameManager _gameManager;

        public GameController(GameManager gameManager)
        {
            _gameManager = gameManager;
        }

        // GET api/ping/with-auth
        [HttpGet("results")]
        [Authorize]
        public ActionResult GetResults()
        {
            try
            {
                List<GameMatch> games = _gameManager.ShowGames();
                return Ok(games);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        // GET api/ping/with-auth
        [HttpGet("ShowGames")]
        public ActionResult GetShowGames()
        {
            try
            {
                List<GameMatch> games = _gameManager.ShowGames();
                return Ok(games);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        // GET api/ping/with-auth
        [HttpPost("play")]

        public async Task<ActionResult> PostPlay(int userId)
        {
            try
            {
                int number = await _gameManager.Play(userId);
                return CreatedAtAction(nameof(GameMatch), new { number = number }, userId);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
